import java.util.Scanner;
public class main {
    public static void main(String[] args) {
        int Selector = 0;
        System.out.println("Welcome to Lab1!. You have 5 exercises.");
        System.out.println("Select an Exercise:");
        Scanner scan=new Scanner(System.in);
        Selector = scan.nextInt();
        switch (Selector) {
            case 1:
                ex1.checkNumber();
                break;
            case 2:
                ex2.sumVector();
                break;
            case 3:
                ex3.nrPrim();
                break;
            case 4:
                ex4.Area rectangle = new ex4.Area(2, 16);
                rectangle.getArea();
                break;
            case 5:
                MyOwnAutoShop.Cars();
                break;
            default:
                System.out.println("Invalid selection");  } }

    public class MyOwnAutoShop {
        public static void Cars() {
            ex5.Sedan sedan = new ex5.Sedan(120, 20000, "red", 18);
            ex5.Ford ford1 = new ex5.Ford(110, 25000, "blue", 2010, 2000);
            ex5.Ford ford2 = new ex5.Ford(130, 30000, "black", 2015, 3000);
            ex5.Car car = new ex5.Car(100, 15000, "white");

            double sedanPrice = sedan.getSalePrice();
            double ford1Price = ford1.getSalePrice();
            double ford2Price = ford2.getSalePrice();
            double carPrice = car.getSalePrice();

            System.out.println("Sedan sale price: " + sedanPrice);
            System.out.println("Ford 1 sale price: " + ford1Price);
            System.out.println("Ford 2 sale price: " + ford2Price);
            System.out.println("Car sale price: " + carPrice);
        }
    }

}

